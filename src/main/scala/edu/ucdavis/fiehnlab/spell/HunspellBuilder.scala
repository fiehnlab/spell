package edu.ucdavis.fiehnlab.spell

import java.io._
import java.util.zip.GZIPInputStream

import com.atlascopco.hunspell.Hunspell
import com.typesafe.scalalogging.LazyLogging
import org.springframework.core.io.support.PathMatchingResourcePatternResolver

/**
  * builds unique hunspell dictionaries from
  * an ontology
  */
object HunspellBuilder extends LazyLogging {

  def build(expression:String):Hunspell = build (new PathMatchingResourcePatternResolver().getResources(expression).map{resource => new GZIPInputStream(resource.getInputStream)}.toList)
  /**
    * builds a list from several input stream
    *
    * @param inputs
    * @return
    */
  def build(inputs: List[InputStream]): Hunspell = {
    val file = File.createTempFile("custom", "disc")
    val writer = new PrintWriter(new FileWriter(file))

    logger.info(s"storing temp file at: ${file.getAbsolutePath}")

    val words = inputs.flatMap { input =>
      try {
        scala.io.Source.fromInputStream(input).getLines().flatMap { line =>
          val row = line.split("\t")

          row.flatMap { entry =>
            entry.split(" ").map(_.trim)
          }


        }.filter(_.contains("http") == false).filter(_ matches "[a-zA-Z]{2,}+").toSet[String]

      } finally {
        input.close()
      }
    }

    writer.println(words.size)
    words.foreach { word =>
      writer.println(word)
    }

    writer.flush()
    writer.close()

    new Hunspell(file.getAbsolutePath, "")
  }

}
