package edu.ucdavis.fiehnlab.spell

import java.io.{File, FileOutputStream}

import com.atlascopco.hunspell.Hunspell
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.rockymadden.stringmetric.StringMetric
import com.rockymadden.stringmetric.similarity.NGramMetric
import com.typesafe.scalalogging.LazyLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.context.annotation.{Bean, Primary}
import org.springframework.util.FileCopyUtils
import org.springframework.web.bind.annotation._

import scala.collection.JavaConverters._

/**
  * Created by wohlgemuth on 2/9/17.
  */
@SpringBootApplication
@EnableDiscoveryClient
class SpellApplication {

  @Bean
  def spellChecks: List[Hunspell] = {
    new Hunspell(buildFile("/en_med_glut.dic", File.createTempFile("en_med_glut", "dic")), buildFile("/en-US.aff", File.createTempFile("/en_med_glut", "aff"))) ::
      new Hunspell(buildFile("/en-US.dic", File.createTempFile("en-US", "dic")), buildFile("/en-US.aff", File.createTempFile("en-US", "aff"))) ::
      new Hunspell(buildFile("/ChemDictOOo.dic", File.createTempFile("ChemDict00o", "dic")), buildFile("/ChemDictOOo.aff", File.createTempFile("/ChemDictOOo", "aff"))) ::
      HunspellBuilder.build("classpath*:/ontobee/*.tsv.gz") ::
      List()
  }

  @Bean
  def metric: StringMetric[Double] = NGramMetric(1)


  /**
    * moves the file from our resources to a temp file
    *
    * @param input
    * @param output
    * @return
    */
  def buildFile(input: String, output: File): String = {
    FileCopyUtils.copy(getClass.getResourceAsStream(input), new FileOutputStream(output))
    output.deleteOnExit()
    output.getAbsolutePath
  }

  @Bean
  @Primary
  def objectMapper: ObjectMapper = {
    val mapper = new ObjectMapper() with ScalaObjectMapper

    mapper.registerModule(DefaultScalaModule)
    mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    mapper.setSerializationInclusion(Include.NON_NULL)
    mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)

    mapper
  }

}


object Application extends App {
  SpringApplication.run(classOf[SpellApplication], args: _*)
}

@CrossOrigin
@RestController
@RequestMapping(value = Array("/rest/spell"))
class SpellController extends LazyLogging {

  @Autowired
  val spellChecks: List[Hunspell] = null

  @Autowired
  val metric: StringMetric[Double] = null

  @RequestMapping(value = Array {
    "/check"
  }, method = Array {
    RequestMethod.POST
  })
  def check(@RequestBody query: String, @RequestParam(name = "minScore", required = false, defaultValue = "0.8") minScore: Double): Array[Result] = {

    doCheck(query, minScore)
  }

  /**
    * does the actual checking and provides a result set for
    *
    * @param query
    * @param minScore
    * @return
    */
  def doCheck(query: String, minScore: Double): Array[Result] = {
    logger.info(s"trying to spell check: ${query}")

    val result = spellChecks.collect {
      case x if x.spell(query) =>
        query :: List()
      case x => x.suggest(query).asScala
    }.flatMap { toScore =>
      toScore.map { result =>
        Result(query, result, metric.compare(query.toLowerCase(), result.toLowerCase()).getOrElse(0.0))
      }
    }.toSet.filter(_.score > minScore).toList.sortBy(_.score).reverse

    logger.info(s"converted: ${result}")

    result.toArray
  }

  @RequestMapping(value = Array {
    "/check/{query}"
  }, method = Array {
    RequestMethod.GET
  })
  def checkByVariable(@PathVariable query: String, @RequestParam(name = "minScore", required = false, defaultValue = "0.8") minScore: Double): Array[Result] = {
    doCheck(query, minScore)
  }

}

case class Result(query: String, result: String, score: Double)

