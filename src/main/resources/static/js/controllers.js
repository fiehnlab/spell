/**
 * Created by matthewmueller on 2/10/17.
 */

(function(angular) {
    var AppController = function($scope, $http) {
        $scope.check = function(query) {
            $http.get('/rest/spell/check/'+query)
                .then(function(response) {
                    $scope.results = response.data;
                });
        };
    };

    AppController.$inject = ['$scope','$http'];
    angular.module("myApp.controllers").controller("AppController", AppController);
}(angular));