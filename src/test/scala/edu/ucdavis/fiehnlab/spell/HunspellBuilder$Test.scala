package edu.ucdavis.fiehnlab.spell

import java.util.zip.GZIPInputStream

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.WordSpec
import org.springframework.core.io.support.PathMatchingResourcePatternResolver

/**
  * Created by wohlgemuth on 2/9/17.
  */
class HunspellBuilder$Test extends WordSpec with LazyLogging{

  "HunspellBuilder$Test" should {

    "build" must {

      "load a stream as TSV " should {
        val streams = new PathMatchingResourcePatternResolver().getResources("classpath*:/ontobee/*.tsv.gz").map{resource => new GZIPInputStream(resource.getInputStream)}.toList

        "build a dictionary object" must {
          val hunspell = HunspellBuilder.build(streams)

          "contain - dorsal" in {
            logger.info(s"${hunspell.suggest("dorsal")}")
            assert(hunspell.spell("dorsal"))
          }
        }
      }
    }

  }
}
