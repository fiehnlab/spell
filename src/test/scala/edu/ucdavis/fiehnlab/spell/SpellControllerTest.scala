package edu.ucdavis.fiehnlab.spell

import com.typesafe.scalalogging.LazyLogging
import org.junit.runner.RunWith
import org.scalatest.{ShouldMatchers, WordSpec}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.test.context.TestContextManager
import org.springframework.test.context.junit4.SpringRunner

/**
  * Created by wohlgemuth on 2/9/17.
  */
@RunWith(classOf[SpringRunner])
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, properties = Array {
  "eureka.client.enabled:false"
})
class SpellControllerTest extends WordSpec with ShouldMatchers with LazyLogging {

  @Autowired
  val restTemplate: TestRestTemplate = null

  // required for spring and scala tests
  new TestContextManager(this.getClass).prepareTestInstance(this)

  "SpellControllerTest" must {

    "check spelling for ulture" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.postForEntity("/rest/spell/check?minScore=0.5", "ulture", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce culture as result " in {
        val list = result.getBody

        assert(list.contains(Result("ulture", "culture", 0.8571428571428571)))
      }
    }

    "check spelling for alanine" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.postForEntity("/rest/spell/check", "alanine", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce alanine as result " in {
        val list = result.getBody

        assert(list.contains(Result("alanine", "alanine", 1.0)))
      }
    }


    "check spelling for Alanine" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.postForEntity("/rest/spell/check", "Alanine", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce alanine as result " in {
        val list = result.getBody

        assert(list.contains(Result("Alanine", "Alanine", 1.0)))
      }
    }


    "check spelling for 4-pyridoxic" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.postForEntity("/rest/spell/check?minScore=0.0", "4-pyridoxic", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce 4-pyridoxic as result " in {
        val list = result.getBody

        assert(list.contains(Result("4-pyridoxic", "4-pyridoxic", 1.0)))
      }
    }
    "check spelling for arabidopsis" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.postForEntity("/rest/spell/check?minScore=0.0", "arabidopsis", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce arabidopsis as result " in {
        val list = result.getBody

        assert(list.contains(Result("arabidopsis", "Arabidopsis", 1.0)))
      }
    }

    "check spelling for arabdopsis" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.postForEntity("/rest/spell/check?minScore=0.0", "arabdopsis", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce arabdopsis as result " in {
        val list = result.getBody

        assert(list.contains(Result("arabdopsis", "Arabidopsis", 0.9090909090909091)))
      }
    }

    "check spelling for musculus" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.postForEntity("/rest/spell/check?minScore=0.0", "musculus", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce musculus as result " in {
        val list = result.getBody

        assert(list.contains(Result("musculus", "musculus", 1.0)))
      }
    }

    "check spelling for musculus using get" should {

      val result: ResponseEntity[Array[Result]] = restTemplate.getForEntity("/rest/spell/check/musculus?minScore=0.0", classOf[Array[Result]])

      "have status code OK" in {
        result.getStatusCode shouldBe HttpStatus.OK
      }
      "not be null" in {
        result.getBody should not be null
      }
      "not be empty" in {
        result.getBody.isEmpty should not be true
      }

      "produce musculus as result " in {
        val list = result.getBody

        assert(list.contains(Result("musculus", "musculus", 1.0)))
      }
    }
  }
}